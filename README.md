# Recipie

This app was created using [MEAN](http://mean.io/) stack as a project for 3rd-year Data Representation module.

My main goal was to create backend as robust as possible. It's why I went the extra mile doing it and spend the majority of the time other there. Probably it's why front end is, as my classmate said, "not very appealing to look at".

In this app back end is independent of front end functionality. The frontend can be easily changed without big effort because it totally separated. Backend handles all related DB updates. So if front end tells "add comment" the backend automatically adds this comment to list of whatever it was commented at as well to the user's comments list. By doing this front end don't need to wary what some updates might be missing.

This project has no JS callbacks I used JS Promises instead because I believe code readability matters. It has autentification implemented using JWT instead of using cookies. JWT has better security. Here is more stuff I did on this project backend but I can't remember it now, at the time I updating this readme.





## Express end points:

`POST: /auth/localLogin` | login user | body.password body.username

`GET: /user/`  | get user details

`POST: /user/` | create user

`PUT: /user/` | update user details

`DELETE: /user/` | remove user

`GET: /user/recipes/` | get user recipes

`GET: /user/comments/` | get user comments

``

jwt token last only 1 h



# References:
- (Angular + pasport)https://www.youtube.com/watch?v=IlpU1z3cvSQ
- (passport + express) https://www.youtube.com/watch?v=CHodPpqLqG8&index=2&list=PL4cUxeGkcC9jdm7QX143aMLAqyM-jTZ2x
- (passportjs) http://www.passportjs.org/
- (MEAN + Angular6) https://auth0.com/blog/real-world-angular-series-part-1/
- (passportjs + github login) https://www.jokecamp.com/tutorial-passportjs-authentication-in-nodejs/
- (Angular + PWA) https://vitalflux.com/angular-6-create-pwa-progressing-web-app/
- (bcryptjs + passpotrjs) https://github.com/apoorvlala/Login-APP-using-Node.js-Expressjs-Passportjs-Bcryptjs-and-MongoDB
- (JWT) https://medium.com/front-end-hacking/learn-using-jwt-with-passport-authentication-9761539c4314
- (mangoose validators) https://github.com/leepowellcouk/mongoose-validator
- (express Link redirect) https://stackoverflow.com/questions/19035373/how-do-i-redirect-in-expressjs-while-passing-some-context
- (Promises create) https://developer.mozilla.org/en-US/docs/Web/JavaScript/Reference/Global_Objects/Promise
- () https://scotch.io/tutorials/javascript-promises-for-dummies
- (Referencing another schema in Mongoose) https://stackoverflow.com/questions/18001478/referencing-another-schema-in-mongoose#18002078
- (remove mongo document references) https://stackoverflow.com/a/11905116/5322506
- (angular inter component comunication) http://jasonwatmore.com/post/2016/12/01/angular-2-communicating-between-components-with-observable-subject
- (multy subscribers to Observables) https://stackoverflow.com/a/43644258/5322506
- (Send data through routing paths in Angular) https://stackoverflow.com/questions/44864303/send-data-through-routing-paths-in-angular
- ()
